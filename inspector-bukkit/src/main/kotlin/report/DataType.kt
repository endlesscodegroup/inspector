package ru.endlesscode.inspector.bukkit.report

enum class DataType {
    CORE, PLUGINS
}

package ru.endlesscode.inspector.report

class ExceptionData(
    val exception: Exception,
    var times: Int = 1
)
